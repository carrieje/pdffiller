import pdffiller

def one():
    directives = [
        {
            'template': 'example/page-one.pdf',
            'overlay': {
                1: [
                    {
                        'text': 'Hello',
                        'x': 100,
                        'y': 100,
                        'fontSize': 12,
                    },
                    {
                        'text': 'World',
                        'x': 100,
                        'y': 80,
                        'fontSize': 20,
                    },
                ],
            },
        },
        {
            'template': 'example/page-two-and-three.pdf',
            'overlay': {
                2: [
                    {
                        'text': 'Nothing to do on first page',
                        'x': 100,
                        'y': 200,
                        'fontSize': 12,
                    },
                ],
            },
        },
    ]
    pdffiller.fill('example/out-1.pdf', directives)

def two():
    collection = pdffiller.Collection()

    template = collection.append('example/page-one.pdf')
    template.add_field('name', page=1, x=100, y=400, fontSize=12)

    template = collection.append('example/page-two-and-three.pdf')
    template.add_field('name', page=1, x=300, y=600, fontSize=20)
    template.add_field('name', page=2, x=50, y=500, fontSize=20)
    template.add_field('age', page=2, x=50, y=450, fontSize=10)

    collection.compile('example/out-2.pdf',
                       {'name': 'John Doe', 'age': '28 years old'})

def three():
    collection = pdffiller.Collection()

    template = collection.append('example/landscape.pdf')
    template.add_field('out', page=1, x=520, y=400, fontSize=22)

    collection.compile('example/out-3.pdf', {'out': 'This is a test!'})

if __name__ == '__main__':
    one()
    two()
    three()
