from pdfrw import PdfReader, PdfWriter, PageMerge
from reportlab.pdfgen import canvas
import io

def fill(out:str, request:list):
    '''
    out is a string. It is a path where to write the output.
    request is a list. Each element is a file with its overlaying directives.
    Each file is concatenated in the order of the list.

    Here is an example of request
    [
        {
            'template': '',
            'overlay': {
                1: [        # On first page
                    {
                        'text': '',
                        'x': 0,
                        'y': 0,
                        'fontSize': 12,
                    },
                    ...
                ],
            },
        },
        ...
    ]
    '''
    writer = PdfWriter()
    for element in request:
        reader = PdfReader(element['template'])
        for n, page in enumerate(reader.pages, start=1):
            if n in element['overlay']:
                overlay(page, directives=element['overlay'][n])
            writer.addpage(page)
        writer.write(out)

def overlay(page, directives:list):
    '''
    directives in a list.

    Here is an example of directives
    [
        {
            'text': '',
            'x': 0,
            'y': 0,
            'fontSize': 12,
        },
        ...
    ]
    '''
    data = io.BytesIO()
    a_canvas = canvas.Canvas(data)

    x, y = page.MediaBox[2:4]
    a_canvas.setPageSize((x,y))

    for directive in directives:
        textobj = a_canvas.beginText(directive['x'], directive['y'])
        textobj.setFont(psfontname= 'Helvetica', size=directive['fontSize'])
        textobj.textOut(directive['text'])
        a_canvas.drawText(textobj)

    a_canvas.save()
    data.seek(0)
    reader = PdfReader(data)
    PageMerge(page).add(reader.pages[0]).render()

