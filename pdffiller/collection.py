import pdffiller

class Template:

    def __init__(self, pdf:str):
        self._path = pdf
        self._fields = {}

    def add_field(self, name:str, page:int, x:int, y:int, fontSize:int):
        new = {'page': page, 'x': x, 'y': y, 'fontSize': fontSize}
        self._fields.setdefault(name, []).append(new)

    def _directives(self, population:dict) -> dict:
        overlay = {}
        for name, value in population.items():
            fields = self._fields.get(name, []) # Silence a potention name error
            for field in fields:
                new = {k:field[k] for k in ['x', 'y', 'fontSize']}
                new['text'] = value
                overlay.setdefault(field['page'], []).append(new)
        return {'template': self._path, 'overlay': overlay}


class Collection:

    def __init__(self):
        self._templates = []

    def append_template(self, template:Template):
        self._templates.append(template)

    def append(self, pdf:str) -> Template:
        template = Template(pdf)
        self._templates.append(template)
        return template

    def compile(self, out:str, population:dict):
        '''
        population is a dict with field names as keys and the
        text to fill them with as value.
        A Template can have multiple occurrences of the same
        field name, and field names are shared accross all
        Template instance of the same Collection.
        All values must be of type str.

        Here is an example of population
        {'name': 'John Doe', 'age': '14'}
        '''
        directives = self._directives(population)
        pdffiller.fill(out, directives)

    def _directives(self, population:dict) -> list:
        directives = []
        for template in self._templates:
            directives.append(template._directives(population))
        return directives
